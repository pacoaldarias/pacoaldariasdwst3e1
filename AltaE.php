<?php
include_once('Config.php');
include_once("funciones.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>



        <?php cabecera(); ?>

        <h3>Resultado del alta</h3>
        <h4>Autor Profesor: Paco Aldarias</h4>

        <?php
        $id = recoge("id");
        if ($id == "")
           $id = "Error: Vacio";

        $nombre = recoge("nombre");
        if ($nombre == "")
           $nombre = "Error: Vacio";

        $url = recoge("url");
        if ($url == "")
           $url = "Error: Vacio";

        $tipoenlace = recoge("tipoenlace");
        if ($tipoenlace == "")
           $tipoenlace = "Error: Vacio";
        ?>

        <table border="1">
            <tr>
                <td>Id</td>
                <td>
                    <?php echo $id; ?>
                </td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td><?php echo $nombre; ?></td>
            </tr>
            <tr>
                <td>Url</td>
                <td><?php echo $url; ?></td>
            </tr>

            <tr>
                <td>Tipo Enlace</td>
                <td><?php echo $tipoenlace; ?></td>
            </tr>



        </table>

        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>

<?php include_once("funciones.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>
        <h3>FORMULARIOS</h3>

        <p>Elegir:</p>
        <ul>
            <li><a href="MenuE.php">Profesor. Gestión de enlaces</a> </li>
            <li><a href="MenuTE.php">Alumno1. Gestión de tipos de enlaces </a> </li>
            <li><a href="MenuU.php">Alumno2. Gestión de usuarios</a> </li>
            <li><a href="MenuTU.php">Alumno3. Gestión de tipos de usuarios</a> </li>
        </ul>

        <?php docu(); ?>
        <?php grupo(); ?>
        <?php pie(); ?>

    </body>
</html>
